# Statuts

## Article 1 — Le nom de l'association

Il est fondé
entre personnes adhérentes aux présents statuts
une association
régie par la loi du 1er juillet 1901 et le décret du 16 août 1901,
ayant pour titre
« la Limule ».

## Article 2 — Objet social

Cette association a vocation à
promouvoir, fédérer et organiser
les activités culturelles, sportives et solidaires
auxquelles participent des doctorantes et doctorants
de l’Institut des Sciences de l’Évolution de Montpellier
(ISEM, ci-après dénommé "le laboratoire").
Toutes les autres catégories de personnels du laboratoire sont bienvenues,
permanentes et non-permanentes,
étudiantes et professionnelles,
en contrat avec le laboratoire,
hébergées au laboratoire
ou en collaboration étroite,
tant que l'activité de l'association
reste dans l'intérêt de la communauté doctorante
ou à son initiative.

## Article 3 — Siège social

Le siège social est fixé à `<X>`.
Il pourra être transféré par simple décision du conseil d’administration;
la ratification par l’assemblée générale sera nécessaire.

## Article 4 — Durée

La durée de l’association est illimitée.

## Article 5 — Composition

L’association se compose de :

- Membres actifs (personnes physiques).
- Membres référents (personnes physiques).
- Anciens membres (personnes physiques).
- Membres fondateurs :
  - M. Iago Bonnici (né le 9 mars 1991 à Pertuis)
  - Mme. Fiona Cornet (née le 26 juillet 1997 à Nantes)
  - M. Théo Deremarque (né le 25 septembre 1997 à Saint-Dié-des-Vosges)
  - M. Fabien Duez (né le 9 décembre 1998 à Valenciennes)
  - Mme. Marie Haut-Labourdette (née le 29 juillet 1999 à Monaco)
  - Mme. Juliette Testas (née le 24 mai 1997 à Paris)

La moitié des membres actifs au moins,
et la moitié des membres référents au moins
doivent être constituées d'étudiants et d'étudiantes
de l'Université de Montpellier.

Les membres fondateurs
sont les premiers membres actifs de l'association,
et fondent le bureau décrit par l'article 11.1
en tant que membres référents,
constituant par là le premier conseil d'administration.
Comme elle est antérieure à la création du conseil,
leur première demande d'admission ne nécessite pas de validation.

Hormis cela, ce statut ne leur confère
aucun droit,
aucune responsabilité
ni aucune prérogative particulière
differents de ceux des autres membres actifs.
En particulier,
ils doivent s'acquitter des autres modalités d'admission décrites à l'article 6,
disposent chacun d'une seule voix au conseil,
et les conditions de renouvellement de leur qualité de membre actif ou référent
sont les mêmes que pour toutes les autres personnes physiques.

## Article 6 — Admission

Pour rejoindre l'association en tant que membre actif,
il faut que le conseil d'administration
juge la demande d'admission conforme
à l'objet social de l'association défini par l'article 2
et aux conditions de composition définies par l'article 3,
puis il faut adhérer au règlement intérieur en vigueur
et s'acquitter de la cotisation annuelle.
Le renouvellement des admissions a lieu chaque année.

Les seules demandes non-conformes à l'article 2
sont les demandes émises par des personnes morales,
par des personnes physiques étrangères au laboratoire
qui ne travaillent pas en collaboration professionnelle étroite
avec des personnels du laboratoire,
et par des personnes précédemment radiées selon l'article 8.

## Article 7 — Les membres

Sont membres actifs
ceux dont la demande est annuellement validée
par le conseil d'administration,
et qui s'acquittent annuellement
d'une somme de 10€ à titre de cotisation.
Les différentes modalités de cotisation
sont décrites par le règlement intérieur.

Sont membres référents
les membres actifs élus par l'assemblée générale
selon les modalités décrites par les articles 10 et 11,
jusqu'à la fin de leur mandat,
la dissolution de leur pôle
ou leur démission.

Sont anciens membres, sans limitation de durée,
ceux qui ont été membres actifs par le passé,
mais ont perdu cette qualité
parce qu'ils ne sont plus admissibles en tant que membre actifs
ou ne s'acquittent plus de leurs cotisations.

## Article 8 — Radiations

La qualité de membre se perd par :
- La démission.
- Le décès.
- La radiation prononcée par le conseil d’administration
  pour motif grave en regard du règlement intérieur,
  la personne intéressée ayant été invitée par lettre recommandée
  à se présenter devant le conseil pour fournir des explications.

## Article 9 — Ressources

Les ressources de l’association comprennent :
- Le montant des cotisations.
- Le montant des droits d’entrée
  des événements organisés.
- Les subventions de l’État,
  des collectivités territoriales,
  des institutions de l’enseignement supérieur et de la recherche
  (notamment les établissements publics
  à caractère scientifique, culturel et professionnel (EPCSP)).
- Les aides en nature et fonds privés.
- Les produits des activités et services.
- Toutes autres ressources
  autorisées par les textes législatifs et réglementaires.

## Article 10 — Conseil d’administration

La direction quotidienne de l'association
est assurée par un conseil d'administration
composé d'au moins 3 membres
et d'au minimum 50% (arrondis au supérieur)
d'étudiants et d'étudiantes de l'Université de Montpellier.
Les membres du conseil sont des membres actifs
élus par l’assemblée générale pour des mandats d'une année
et rééligibles.

Le conseil est constitué de l'ensemble des membres référents
de chacun des pôles définis par l'article 11.
Aucun membre du conseil n'est pas référent d'un pôle.

Le conseil se réunit une fois au moins tous les 6 mois,
ou à la demande du quart de ses membres.

### Article 10.1 — Modalités de décision du conseil d'administration

Les décisions du conseil
sont prises au jugement majoritaire.
Elles peuvent être prises plus simplement à la majorité des voix
à moins qu'un quart des membres présents ne s'y oppose.

Chaque membre du conseil présent dispose d'une et une seule voix,
et ne peut pas s'abstenir.

Le vote est à scrutin secret
si un quart des membres présents
ou 10 membres présents
le demandent.
Le vote est systématiquement à scrutin secret
s’il porte sur des personnes physiques.

Les décisions ne sont valides
que si la moitié des membres est présente
au moins.

## Article 11 — Les pôles

La gestion quotidienne de l'association
est assurée par ses membres.
Si nécessaire, elle peut s'organiser
en pôles thématiques,
missionnés par le conseil d'administration
pour répondre à des besoins particuliers.
Un pôle est composé de membres volontaires
et de membre référents,
il répond à un objet précis et immuable,
spécifié lors de sa création,
et il est automatiquement dissout
lorsque son objet prend fin.

Les membres référents des pôles
constituent la totalité du conseil d'administration.
Aucun membre ne peut être référent de plus d'un pôle à la fois.
Chaque pôle compte au minimum 1 membre référent
et au maximum 10 membres référents.
Chaque année,
l'assemblée générale procède au renouvellement
des membres référents de chaque pôle.

La création d'un nouveau pôle
est proposée par le conseil d'administration,
qui rédige l'objet dudit pôle
puis le soumet à l'assemblée générale.
L'assemblée vote la création du pôle
et en choisit les premiers membres référents.

En cas de vacance d'un membre référent,
son pôle pourvoit à son remplacement provisoire.
Il est procédé à son remplacement définitif
lors de l'assemblée générale suivante.
Les pouvoirs des membres référents provisoires prennent fin
à l’expiration du mandat des membres qu’ils remplacent.

La gouvernance interne de chaque pôle
est laissée à la discrétion de ses membres,
tant qu'elle reste conforme au règlement intérieur.

### Article 11.1 Le bureau

L'association est créée avec un seul pôle initial nommé "bureau",
composé des membres fondateurs
et répondant à l'objet suivant:

> *Le bureau est chargé
> de la gestion administrative quotidienne de l'association,
> répartie en trois tâches :*
> - *Présidence: représentation légale de l'association, responsabilité.*
> - *Trésorerie: gestion financière de l'association.*
> - *Secrétariat: dialogue avec les administrations tierces;
>                 édition, publication et archivage
>                 des comptes-rendus et procès-verbaux
>                 des réunions du conseil et de l'assemblée.*
>
> *Le bureau choisit en son sein un seul membre
> chargé de la signature au nom de la Présidence.
> En cas de vacance, ce membre délègue sa signature
> au titre du remplacement provisoire décrit par l'article 11.  
> De la même manière, le bureau choisit en son sein un seul membre
> chargé des accès aux comptes de l'association au nom de la Trésorerie,
> qui délègue ses accès en cas de vacance.*


## Article 12 — Assemblée générale ordinaire

L’assemblée générale ordinaire
comprend tous les membres actifs de l’association.
Elle se réunit chaque année entre janvier et mars.

Toute l'année,
les membres de l'association soumettent au conseil
leurs suggestions pour l'ordre du jour.
Le conseil d'administration rédige l'ordre du jour.
Quinze jours au moins avant la date fixée,
les membres actifs de l’association
sont convoqués par le conseil
qui leur communique l'ordre du jour.
Seules les questions soumises à l'ordre du jour
seront traitées lors de l'assemblée.

Le conseil d'administration
choisit un membre référent
pour présider l’assemblée,
exposer la situation morale
et l’activité de l’association.
Les membres chargés de trésorerie
rendent compte de la gestion
et soumettent le bilan financier
à l’approbation de l’assemblée.
Les membres chargés du secrétariat
prennent note des discussions
et décisions de l'assemblée
pour en constituer une archive publique.

Après épuisement de l'ordre du jour,
il est procédé au remplacement
des membres sortants du conseil.

### Article 12.1 — Modalités de décision de l'assemblée générale.

Les décisions de l'assemblée générale
sont prises au jugement majoritaire.
Elles peuvent être prises plus simplement à la majorité des voix
à moins qu'un quart des membres présents ne s'y oppose.

Chaque membre actif dispose d'une et une seule voix,
et peut choisir de s'abstenir.
Un membre absent peut donner procuration
à un membre actif présent pour voter à sa place.
Un membre ne peut pas recevoir
plus de deux voix par procuration.

Le vote est à scrutin secret
si un quart des membres présents
ou 10 membres présents
le demandent.
Le vote est systématiquement à scrutin secret
s’il porte sur des personnes physiques.

Les décisions ne sont valides
que si la moitié au moins des membres présents exprime son vote,
et si le total des votes exprimés,
en présence ou par procuration,
représente au moins le quart
des membres actifs de l'association.

## Article 13 — Assemblée générale extraordinaire

Au besoin,
ou à la demande de plus de la moitié des membres actifs,
le conseil d'administration
convoque une assemblée générale extraordinaire,
suivant les modalités prévues aux présents statuts,
et uniquement pour
modification des statuts,
création de nouveaux pôles,
dissolution de pôles,
édition du règlement intérieur,
actes portant sur des immeubles
ou dissolution de l'association.
Les modalités de convocation et de décision
sont les mêmes que pour l’assemblée générale ordinaire.

## Article 14 — Indemnités

Toutes les fonctions des membres,
y compris celles des membres référents,
sont gratuites et bénévoles.
Seuls les frais occasionnés
par l’accomplissement de leur mandat
sont remboursés sur justificatifs.
En particulier,
les cotisations ne sont pas remboursées
en cas de démission ou de radiation.
Le rapport financier,
présenté à l’assemblée générale ordinaire,
détaille, par bénéficiaire,
les remboursements
de frais de mission,
de déplacement
ou de représentation.

## Article 15 — Règlement intérieur

Un règlement intérieur est établi,
puis édité par le conseil d’administration,
qui le fait approuver par l’assemblée générale.

## Article 16 — Dissolution

En cas de dissolution
prononcée par les deux tiers au moins
des membres présents à l’assemblée générale,
un ou plusieurs liquidateurs sont nommés par celle-ci
et l’actif est, s’il y a lieu,
dévolu conformément aux lois et règlements en cours.



Fait à `<X>`, le `<X>`.
