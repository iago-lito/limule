
### Cotisations

Pour être membre actif·ve de l'association,
il faut s'acquitter d'une cotisation annuelle de 10€.

Les cotisations réglées
entre les mois d'__octobre__ et de __janvier__ (inclus)
offrent la qualité de membre actif·ve
jusqu'au 30 septembre suivant.
Les cotisations réglées
entre les mois de __février__ et de __septembre__ (inclus)
offrent la qualité de membre actif·ve
jusqu'au 31 janvier suivant.

Il existe trois modalités de cotisation :

- __Standard__: le·la membre règle 10€ à l'association.
  Optionnellement,
  iel donne aussi 5€
  ou n'importe quel montant supplémentaire
  pour alimenter la cagnotte de "cotisations solidaires"
  tenue par la trésorerie.
  Dans cette cagnotte, chaque euro compte pour 1/10ème de cotisation solidaire.

- __Solidaire__: sur simple demande du·de la membre,
  et si la cagnotte le permet,
  l'association dépense une cotisation solidaire
  pour l'acquitter de sa cotisation.

- __Prolongatoire__: à la demande du·de la membre,
  l'association octroie une cotisation prolongatoire
  qui le·la dispense de cotisation pour la période en cours.
  La demande est refusée si le·la membre
  n'a jamais cotisé de façon standard ou solidaire,
  ou si iel a déjà bénéficié
  d'une cotisation prolongatoire par le passé,
  à moins qu'iel ne s'en acquitte rétroactivement
  par une cotisation standard ou solidaire.
  La demande est automatique.
