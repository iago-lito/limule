Un dépôt pour organiser les discussions
autour de l'association des doctorant·es de l'ISEM.

Les archives [git] proprement dites peuvent être utiles
pour garder trace de tout changement des statuts,
PVs, comptes-rendus, listes de membres, comptes, *etc.*

Par dessus ça, les outils gitlab ([issues], [issues board], [CI/CD]..)
peuvent être utiles pour commenter, débattre,
garder le fil, produire les documents administratifs nécessaires
à la vie de l'association.

L'association n'est pas encore créée.
Les statuts sont en cours de construction
dans le fichier [`./statuts.md`](./statuts.md).
Discussions
[ici](https://gitlab.com/iago-lito/limule/-/issues/2).

Overview générale de l'état des travaux
[ici](https://gitlab.com/iago-lito/limule/-/boards).

Rendu des statuts
[ici](https://gitlab.com/iago-lito/limule/-/jobs/artifacts/main/raw/render/statuts.pdf?job=render-statuts).

[git]: https://git-scm.com/
[issues]: https://docs.gitlab.com/ee/user/project/issues/
[issues board]: https://docs.gitlab.com/ee/user/project/issue_board.html
[CI/CD]: https://docs.gitlab.com/ee/ci/
