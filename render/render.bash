# Delete title line.
sed 1d ../statuts.md > statuts.out
# Let LaTeX handle numbering.
perl -pi -e 's/Article [\d.]+ — //g' statuts.out
# Remove one subsection nesting level.
perl -pi -e 's/^#//g' statuts.out

# Convert to LaTeX.
pandoc --read=commonmark+smart --write=latex --wrap=none --output=statuts.tex ./statuts.out

# Rough heuristic introduction of non-breaking spaces.
months="janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre"
ordinals="(?:[eè][rm]|nd)e?s?"
article="[Aa]rticles?"
perl -pi -e 's/(\d+'${ordinals}')\s+/$1~/g' statuts.tex
perl -pi -e 's/\s+('${months}')/~$1/g' statuts.tex
perl -pi -e 's/('${article}')\s+([\d.]+)/$1~$2/g' statuts.tex
perl -pi -e 's/(?<!'${months}')\s+(\d+)€?\b(?!'${ordinals}'|~)/~$1/g' statuts.tex

lualatex --halt-on-error template.tex
mv template.pdf statuts.pdf
